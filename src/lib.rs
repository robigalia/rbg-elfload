// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

#![no_std]

pub extern crate xmas_elf;
extern crate zero;

use xmas_elf::{P32, P64, header, program};
use xmas_elf::header::{HeaderPt1, HeaderPt2, HeaderPt2_, Class, MAGIC};
use xmas_elf::program::{ProgramHeader, ProgramHeader32, ProgramHeader64};

use core::ops::Deref;
use core::mem::size_of;
use zero::read;

/// Protect value from being implicitly dropped.
pub struct DropGuard<T> {
    val: T
}

impl<T> Drop for DropGuard<T> {
    fn drop(&mut self) {
        panic!("Drop guard activated: fix your code to not use the implicit `Drop` implementation");
    }
}

impl<T> DropGuard<T> {
    /// Unwrap this guard, preventing the guard from panicing.
    pub unsafe fn unwrap(self) -> T {
        let val = core::ptr::read(&self.val);
        core::mem::forget(self);
        val
    }
}

/// ELF file
///
/// This trait represents an ELF file that will be loaded, and methods of accessing slices into the
/// ELF file as-needed.
///
/// The `Bytes` type is a handle to a sub-slice of the file data. This is used for lazily mapping
/// only the portions of the ELF file that need to be parsed. Feel free to store a reference to the
/// `ElfFile` in it for dropping, as these handles are only created for a limited time and with a
/// strict stack discipline. The deref implementation may be invoked multiple times, so it should
/// be very cheap and return the same slice every time.
///
/// If possible, ensure that the starting address of every returned slice is usize-aligned.
pub trait ElfFile<'a> {
    type Bytes: Deref<Target = [u8]> + 'a;

    fn get_n_bytes_at(&'a self, n: usize, off: usize) -> Option<Self::Bytes>;
}

impl<'a> ElfFile<'a> for &'a [u8] {
    type Bytes = &'a [u8];

    fn get_n_bytes_at(&self, n: usize, off: usize) -> Option<&'a [u8]> {
        Some(&self[off..off+n])
    }
}

#[derive(Debug, PartialEq)]
pub enum LoaderError {
    WrongElfType(header::Type),
    UnknownFormat(&'static str),
    NeedsInterp,
    BadMagic,
    NoMachineClass,
    OutOfBounds,
}

pub trait LoaderErrorContainer {
    fn loader_err(err: LoaderError) -> Self;
}

impl LoaderErrorContainer for LoaderError {
    fn loader_err(err: LoaderError) -> Self { err }
}

/// ELF loader callbacks
///
/// This trait represents places that architecture- or platform-specific code can hook into the ELF
/// loading process, to influence its decisions.
pub trait LoaderCallbacks<'a, F: ElfFile<'a>> {
    type Err: LoaderErrorContainer;
    fn check_ident(&self, _: &F, _: &HeaderPt1) -> Result<(), Self::Err> { Ok(()) }
    fn process_arch_phdr(&self, _: &F, _: ProgramHeader) -> Result<(), Self::Err> { Ok(()) }
    fn pre_load<'b, I: Iterator<Item=ProgramHeader<'b>>>(&self, _: &F, _: &HeaderPt1, _: &HeaderPt2, _: I) -> Result<(), Self::Err> { Ok(()) }
    fn load_segment(&self, _: &F, _: &HeaderPt1, _: &HeaderPt2, _: ProgramHeader) -> Result<(), Self::Err> { Ok(()) }
    fn post_load(&self, _: &F, _: &HeaderPt1, _: &HeaderPt2) -> Result<(), Self::Err> { Ok(()) }
}

impl<'a, F: ElfFile<'a>> LoaderCallbacks<'a, F> for () { 
    type Err = LoaderError;
}

macro_rules! err {
    ($err:ident, $e:expr) => (return Err($err::Err::loader_err($e)))
}

macro_rules! try_or {
    ($e:expr, $($rest:tt)*) => { match $e { Some(s) => s, None => err!($($rest)*) } }
}

pub fn load<'a, F: ElfFile<'a>+'a, C: LoaderCallbacks<'a, F>>(file: &'a F, cbs: C) -> Result<(), C::Err> {
    use LoaderError::*;

    let size_pt1 = size_of::<HeaderPt1>();
    let pt1_root = try_or!(file.get_n_bytes_at(size_pt1, 0), C, OutOfBounds);
    let pt1: &HeaderPt1 = read(&*pt1_root);

    let size_pt2 = match pt1.class {
        Class::None => err!(C, NoMachineClass),
        Class::ThirtyTwo => {
            size_of::<HeaderPt2_<P32>>()
        }
        Class::SixtyFour => {
            size_of::<HeaderPt2_<P64>>()
        }
    };

    if pt1.magic != MAGIC {
        err!(C, BadMagic);
    }

    let pt2_root = try_or!(file.get_n_bytes_at(size_pt2, size_pt1), C, OutOfBounds);

    let pt2 = match pt1.class {
        Class::None => unreachable!(),
        Class::ThirtyTwo => {
            HeaderPt2::Header32(read(&*pt2_root))
        }
        Class::SixtyFour => {
            HeaderPt2::Header64(read(&*pt2_root))
        }
    };

    match pt2.type_().as_type() {
        header::Type::Executable | header::Type::SharedObject => {

        },
        c => err!(C, WrongElfType(c)),
    }

    let ph_entry_size = match pt2 {
        HeaderPt2::Header32(_) => {
            size_of::<ProgramHeader32>()
        },
        HeaderPt2::Header64(_) => {
            size_of::<ProgramHeader64>()
        },
        _ => unreachable!()
    };

    if ph_entry_size != pt2.ph_entry_size() as usize {
        err!(C, UnknownFormat("ph_entry_size disagrees with size of ProgramHeader type"));
    }

    try!(cbs.check_ident(file, pt1));

    let phoff = pt2.ph_offset();
    let pht_root = try_or!(file.get_n_bytes_at(phoff as usize, 
                                               pt2.ph_entry_size() as usize * pt2.ph_count() as usize), 
                           C, OutOfBounds);

    let pt_map = &|d| {
        let pt1 = &pt1;
        match pt1.class {
            Class::None => unreachable!(),
            Class::ThirtyTwo => ProgramHeader::Ph32(read(d)),
            Class::SixtyFour => ProgramHeader::Ph64(read(d)),
        }
    };

    for ph in pht_root.chunks(pt2.ph_entry_size() as usize).map(pt_map) {
        match ph.get_type() {
            program::Type::Interp => err!(C, NeedsInterp),
            program::Type::OsSpecific(_) | program::Type::ProcessorSpecific(_) => {
                try!(cbs.process_arch_phdr(file, ph));
            },
            _ => { }
        }
    }
    
    try!(cbs.pre_load(file, pt1, &pt2, pht_root.chunks(pt2.ph_entry_size() as usize).map(pt_map)));

    for ph in pht_root.chunks(pt2.ph_entry_size() as usize).map(pt_map) {
        if ph.get_type() != program::Type::Load { continue; }

        try!(cbs.load_segment(file, pt1, &pt2, ph));
    }

    try!(cbs.post_load(file, pt1, &pt2));

    Ok(())
}
