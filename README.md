# rbg-elfload

[![Crates.io](https://img.shields.io/crates/v/rbg-elfload.svg?style=flat-square)](https://crates.io/crates/rbg-elfload)

[Documentation](https://doc.robigalia.org/i686/rbg_elfload)

ELF loader, vaguely generic over operations it needs to do.

## Status

Incomplete, under active development.
